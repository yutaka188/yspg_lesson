<?php

print 'Hello World';
//コメントアウトと文（ステートメント）について
// ダブルスラッシュを外せば下記のコードは実行されます。
//print 'Hello World';
print 'Hello World<br>';
//コメントです
if (true) {
  # code...
  # 下記の/* */を削除すれば普通にコードが実行されます。
  /*
  echo "moji<br>";
  echo "moji2<br>";
  echo "moji3<br>";
  */
}